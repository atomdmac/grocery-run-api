const USER = {
  BULK_CREATE: 'user.bulk.create',
  CREATE: 'user.create',
  DEACTIVATE: 'user.deactivate',
  DELETE: 'user.delete',
  REACTIVATE: 'user.reactivate',
  UPDATE: 'user.update',
};

module.exports = {
  USER,
};
