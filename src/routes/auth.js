const Router = require('koa-router');
const handlers = require('../handlers/auth');

const router = new Router({
  prefix: '/auth',
});

router.post('/register', handlers.register);
router.post('/login', handlers.login);
router.post('/logout', handlers.logout);

module.exports = router;
