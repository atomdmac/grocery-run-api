const Router = require('koa-router');
const handlers = require('../handlers/webhooks');

const router = new Router();

router.post('/webhooks', handlers.webhook);

module.exports = router;
