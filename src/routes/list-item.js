const Router = require('koa-router');
const router = new Router();
const {
  ListItem: { create, validateCreate, validateArchive, archive },
} = require('../handlers');

router.post('/list/:listId/add', validateCreate, create);
router.post('/list/:listId/archive/:listItemId', validateArchive, archive);

module.exports = router;
