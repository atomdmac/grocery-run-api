const Router = require('koa-router');
const router = new Router();
const {
  Catalog: {
    addCatalog,
    addCatalogItem,
    archiveCatalog,
    archiveCatalogItem,
    read,
    readAll,
    validateRead,
    validateAddCatalog,
    validateAddCatalogItem,
    searchCatalog,
    validateSearchCatalog,
    searchCatalogItems,
    validateSearchCatalogItems,
  },
} = require('../handlers');

router.get('/catalog/:id', validateRead, read);
router.get('/catalogs', readAll);
router.post('/catalog/add', validateAddCatalog, addCatalog);
router.post('/catalog/:id/add', validateAddCatalogItem, addCatalogItem);
router.post('/catalog/:id/archive', archiveCatalog);
router.post(
  '/catalog/:catalogId/item/:catalogItemId/archive',
  archiveCatalogItem
);
router.get('/catalogs/search', validateSearchCatalog, searchCatalog);
router.get(
  '/catalogs/:catalogId/search',
  validateSearchCatalogItems,
  searchCatalogItems
);

module.exports = router;
