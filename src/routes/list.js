const Router = require('koa-router');
const router = new Router();
const {
  List: {
    archive,
    validateArchive,
    create,
    validateCreate,
    read,
    readAll,
    validateRead,
    update,
    validateUpdate,
  },
} = require('../handlers');

router.get('/lists', readAll);
router.get('/list/:listId', validateRead, read);
router.post('/list/add', validateCreate, create);
router.post('/list/:listId/update', validateUpdate, update);
router.post('/list/:listId/archive', validateArchive, archive);

module.exports = router;
