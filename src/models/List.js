'use strict';

const { Model } = require('objection');

class List extends Model {
  static get tableName() {
    return 'Lists';
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['name'],
      properties: {
        id: { type: 'integer' },
        name: { type: 'string' },
        created_at: { type: 'datetime' },
        updated_at: { type: 'datetime' },
        deleted_at: { type: 'datetime' },
      },
    };
  }

  static get relationMappings() {
    const ListItem = require('./ListItem');

    return {
      listItems: {
        relation: Model.HasManyRelation,
        modelClass: ListItem,
        join: {
          from: 'Lists.id',
          to: 'ListItems.listId',
        },
      },
    };
  }
}

module.exports = List;
