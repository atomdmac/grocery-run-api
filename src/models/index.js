const { Model, QueryBuilder } = require('objection');
const Knex = require('knex');

const dbClient = Knex({
  client: 'pg',
  useNullAsDefault: true,
  connection: {
    host: process.env.POSTGRES_HOST,
    user: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    database: process.env.POSTGRES_DB,
  },
});

// knex.on('query', (error, obj) => console.log(error, obj));

Model.knex(dbClient);

class SoftDeleteQueryBuilder extends QueryBuilder {
  constructor(modelClass) {
    super(modelClass);

    this.onBuild((builder) => {
      const canBeSoftDeleted = modelClass.jsonSchema.properties.deleted_at;

      if (!builder.context().withArchived && canBeSoftDeleted) {
        builder.whereNull(`${modelClass.tableName}.deleted_at`);
      }
    });
  }

  withArchived(withArchived) {
    this.context().withArchived = withArchived;
    return this;
  }

  softDelete() {
    return this.patch({ deleted_at: new Date().toISOString() });
  }

  undelete() {
    return this.withArchived(true).patch({ deleted_at: null });
  }
}

Model.QueryBuilder = SoftDeleteQueryBuilder;
Model.RelatedQueryBuilder = SoftDeleteQueryBuilder;

module.exports = {
  dbClient,
  Catalog: require('./Catalog'),
  CatalogItem: require('./CatalogItem'),
  List: require('./List'),
  ListItem: require('./ListItem'),
  User: require('./User'),
};
