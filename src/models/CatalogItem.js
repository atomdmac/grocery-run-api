'use strict';

const { Model } = require('objection');

class CatalogItem extends Model {
  static get tableName() {
    return 'CatalogItems';
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['name'],
      properties: {
        id: { type: 'integer' },
        name: { type: 'string' },
        catalogId: { type: 'integer' },
        created_at: { type: 'datetime' },
        updated_at: { type: 'datetime' },
        deleted_at: { type: 'datetime' },
      },
    };
  }

  static get relationMappings() {
    const Catalog = require('./Catalog');

    return {
      Catalogs: {
        relation: Model.HasOneRelation,
        modelClass: Catalog,
        join: {
          from: 'CatalogItems.catalogId',
          to: 'Catalogs.id',
        },
      },
    };
  }
}

module.exports = CatalogItem;
