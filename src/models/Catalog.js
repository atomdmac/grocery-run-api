'use strict';

const { Model } = require('objection');

class Catalog extends Model {
  static get tableName() {
    return 'Catalogs';
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['name'],
      properties: {
        id: { type: 'integer' },
        name: { type: 'string' },
        created_at: { type: 'datetime' },
        updated_at: { type: 'datetime' },
        deleted_at: { type: 'datetime' },
      },
    };
  }

  static get relationMappings() {
    const CatalogItem = require('./CatalogItem');

    return {
      catalogItems: {
        relation: Model.HasManyRelation,
        modelClass: CatalogItem,
        join: {
          to: 'Catalogs.id',
          from: 'CatalogItems.catalogId',
        },
      },
    };
  }
}

module.exports = Catalog;
