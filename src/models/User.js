'use strict';

const { Model } = require('objection');

class User extends Model {
  static get tableName() {
    return 'Users';
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['auth_provider_id'],
      properties: {
        id: { type: 'integer' },
        auth_provider_id: { type: 'uuid' },
        status: { type: 'string' },
        created_at: { type: 'datetime' },
        updated_at: { type: 'datetime' },
        deleted_at: { type: 'datetime' },
      },
    };
  }
}

module.exports = User;
