'use strict';

const { Model } = require('objection');

class ListItem extends Model {
  static get tableName() {
    return 'ListItems';
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['listId', 'catalogItemId'],
      properties: {
        id: { type: 'integer' },
        listId: { type: 'integer' },
        catalogItemId: { type: 'integer' },
        quantity: { type: 'integer' },
        created_at: { type: 'datetime' },
        updated_at: { type: 'datetime' },
        deleted_at: { type: 'datetime' },
      },
    };
  }

  static get relationMappings() {
    const CatalogItem = require('./CatalogItem');
    const List = require('./List');

    return {
      list: {
        relation: Model.BelongsToOneRelation,
        modelClass: List,
        join: {
          from: 'ListItems.listId',
          to: 'Lists.id',
        },
      },
      catalogItem: {
        relation: Model.BelongsToOneRelation,
        modelClass: CatalogItem,
        join: {
          from: 'ListItems.catalogItemId',
          to: 'CatalogItems.id',
        },
      },
    };
  }
}

module.exports = ListItem;
