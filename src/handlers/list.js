const Joi = require('joi');
const validate = require('../middleware/validation');
const { Catalog, dbClient, List, ListItem } = require('../models');

const validateCreate = validate({
  body: Joi.object({
    name: Joi.string().required(),
    catalogId: Joi.number().required(),
  }),
});

const create = async (ctx) => {
  const {
    request: { body },
    user,
  } = ctx;
  const catalog = await Catalog.query()
    .select('id')
    .where({ id: body.catalogId, ownedBy: user.id });
  if (catalog.length < 1) {
    ctx.status = 404;
    return;
  }
  const result = await List.query().insert({
    catalogId: body.catalogId,
    name: body.name,
    ownedBy: user.id,
  });
  ctx.body = result;
};

const validateRead = validate({
  params: Joi.object({
    listId: Joi.number().required(),
  }),
});

const read = async (ctx) => {
  const result = await List.query()
    .select('*')
    .where({
      id: ctx.params.listId,
      ownedBy: ctx.user.id,
    })
    .withGraphFetched('[listItems, listItems.catalogItem]')
    .first();

  if (!result) {
    ctx.status = 404;
  } else {
    ctx.body = result;
  }
};

const readAll = async (ctx) => {
  const result = await List.query().select('*').where({
    ownedBy: ctx.user.id,
  });
  ctx.body = result;
};

const validateArchive = validate({
  params: Joi.object({
    listId: Joi.number().integer().required(),
  }),
});

const archive = async (ctx) => {
  await dbClient.transaction(async (trx) => {
    const list = await List.query(trx).softDelete().where({
      id: ctx.params.listId,
      ownedBy: ctx.user.id,
    });
    await ListItem.query(trx).softDelete().where({
      listId: ctx.params.listId,
    });

    if (list > 0) {
      ctx.status = 200;
    } else {
      ctx.status = 404;
    }
  });
};

const validateUpdate = validate({
  params: Joi.object({
    listId: Joi.number().integer().required(),
  }),
  body: Joi.object({
    name: Joi.string().required(),
  }),
});

const update = async (ctx) => {
  const {
    params: { listId },
    request: {
      body: { name },
    },
    user,
  } = ctx;

  const list = await List.query()
    .update({
      name,
    })
    .where({
      id: listId,
      ownedBy: user.id,
    });

  if (list < 1) {
    ctx.status = 404;
    return;
  }

  ctx.status = 200;
};

module.exports = {
  create,
  validateCreate,
  read,
  validateRead,
  readAll,
  validate,
  archive,
  validateArchive,
  update,
  validateUpdate,
};
