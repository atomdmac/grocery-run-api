const { OAUTH_WEBHOOK_KEY } = process.env;
const EVENTS = require('../webhook-events');

const { User } = require('../models');

const createUser = async (ctx) => {
  const {
    request: {
      body: { event },
    },
  } = ctx;
  await User.query().insert({
    auth_provider_id: event.user.id,
  });
  ctx.body = 'ok';
};

const webhook = async (ctx) => {
  console.log('WEBHOOK INCOMING: ', ctx.request.body);

  if (ctx.request.headers['x-groceryrun-auth-key'] !== OAUTH_WEBHOOK_KEY) {
    ctx.status = 401;
    return;
  }

  switch (ctx.request.body.event.type) {
    case EVENTS.USER.CREATE:
      await createUser(ctx);
  }

  ctx.status = 201;
};

module.exports = {
  createUser,
  webhook,
};
