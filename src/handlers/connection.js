const Router = require('koa-router');
const router = new Router();

const ping = async (ctx, next) => {
  ctx.body = 'PONK!';
};
router.get('/ping', ping);

module.exports = router;
