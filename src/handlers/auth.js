const fetch = require('node-fetch');
const {
  AUTH_SERVICE_URL,
  AUTH_SERVICE_LOGIN_URL,
  AUTH_SERVICE_REGISTER_URL,
  AUTH_SERVICE_APPLICATION_ID,
  AUTH_SERVICE_API_KEY,
} = process.env;

const { User } = require('../models');

const register = async (ctx) => {
  try {
    const authResponse = await fetch(
      `${AUTH_SERVICE_URL}${AUTH_SERVICE_REGISTER_URL}`,
      {
        method: 'POST',
        body: JSON.stringify({
          registration: {
            applicationId: AUTH_SERVICE_APPLICATION_ID,
          },
          user: {
            email: ctx.request.body.email,
            password: ctx.request.body.password,
          },
        }),
        headers: {
          'Content-Type': 'application/json',
          Authorization: AUTH_SERVICE_API_KEY,
        },
      }
    );

    const authData = await authResponse.json();

    ctx.cookies.set('access_token', authData.token);
    ctx.cookies.set('refresh_token', authData.refreshToken);

    ctx.status = authResponse.status;
    ctx.body = ctx.status < 400 ? 'welcome' : 'An error occurred';
  } catch (error) {
    ctx.status = 401;
    ctx.body = 'Not Authorized.';
  }
};

const login = async (ctx) => {
  const authResponse = await fetch(
    `${AUTH_SERVICE_URL}${AUTH_SERVICE_LOGIN_URL}`,
    {
      method: 'POST',
      body: JSON.stringify({
        loginId: ctx.request.body.loginId,
        password: ctx.request.body.password,
        applicationId: AUTH_SERVICE_APPLICATION_ID,
      }),
      headers: { 'Content-Type': 'application/json' },
    }
  );

  const authData = await authResponse.json();

  ctx.cookies.set('access_token', authData.token);
  ctx.cookies.set('refresh_token', authData.refreshToken);

  let userRecord = await User.query()
    .select('id')
    .where('auth_provider_id', '=', authData.user.id)
    .first();

  if (!userRecord) {
    ctx.status = 400;
    ctx.message = "User doesn't exist.";
  }

  ctx.body = 'welcome';
};

const logout = async (ctx) => {
  const accessToken = ctx.cookies.get('access_token');
  const refreshToken = ctx.cookies.get('refresh_token');

  const headers = {
    'Set-Cookie': [
      `access_token=${accessToken}`,
      `refresh_token=${refreshToken}`,
    ].join(','),
  };

  const authResponse = await fetch(
    `${AUTH_SERVICE_URL}/logout?global=true&refreshToken=${refreshToken}`,
    {
      method: 'POST',
      headers,
    }
  );

  if (!authResponse.ok) {
    ctx.status = 504;
    ctx.body = 'Dang... something went wrong...';
    return;
  }

  // Tell the client to delete their cookies.
  ctx.cookies.set('access_token', null, { maxAge: 0, overwrite: true });
  ctx.cookies.set('refresh_token', null, { maxAge: 0, overwrite: true });

  ctx.status = 200;
  ctx.body = 'See ya!';
};

module.exports = {
  register,
  login,
  logout,
};
