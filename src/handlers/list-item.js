const Joi = require('joi');
const validate = require('../middleware/validation');
const { CatalogItem, List, ListItem } = require('../models');

const validateCreate = validate({
  params: Joi.object({
    listId: Joi.number().integer().required(),
  }),
  body: Joi.object({
    catalogItemId: Joi.number().integer().required(),
  }),
});

const create = async (ctx) => {
  const {
    params,
    request: { body },
    user,
  } = ctx;

  const list = await List.query()
    .select('id', 'catalogId')
    .where({
      id: params.listId,
      ownedBy: user.id,
    })
    .first();

  if (!list) {
    ctx.status = 404;
    return;
  }

  const catalogItem = await CatalogItem.query()
    .where({
      id: body.catalogItemId,
    })
    .whereExists(
      CatalogItem.relatedQuery('Catalogs').where({
        id: list.catalogId,
        ownedBy: user.id,
      })
    )
    .first();

  if (!catalogItem) {
    ctx.status = 404;
    return;
  }

  await ListItem.query().insert({
    listId: list.id,
    catalogItemId: catalogItem.id,
  });

  ctx.status = 201;
};

const validateArchive = validate({
  params: Joi.object({
    listId: Joi.number().integer().required(),
    listItemId: Joi.number().integer().required(),
  }),
});

const archive = async (ctx) => {
  const {
    params: { listId, listItemId },
    user,
  } = ctx;

  const list = await List.query()
    .select('id')
    .where({
      id: listId,
      ownedBy: user.id,
    })
    .first();

  if (!list) {
    ctx.status = 404;
    return;
  }

  const listItem = await ListItem.query().softDelete().where({
    id: listItemId,
    listId: listId,
  });

  if (listItem > 0) {
    ctx.status = 200;
  } else {
    ctx.status = 404;
  }
};

module.exports = {
  create,
  validateCreate,
  validate,
  archive,
  validateArchive,
};
