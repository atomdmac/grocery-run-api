const Joi = require('joi');
const validate = require('../middleware/validation');
const { Catalog, CatalogItem } = require('../models');

const validateRead = validate({
  params: Joi.object({
    id: Joi.number(),
  }),
});
const read = async (ctx) => {
  const result = await Catalog.query()
    .where({
      id: ctx.params.id,
      ownedBy: ctx.user.id,
    })
    .select('*')
    .withGraphFetched('catalogItems')
    .first();

  if (!result) {
    ctx.status = 404;
  } else {
    ctx.body = result;
  }
};

const readAll = async (ctx) => {
  const result = await Catalog.query().select('*').where({
    ownedBy: ctx.user.id,
  });
  ctx.body = result;
};

const validateAddCatalog = validate({
  body: Joi.object({
    name: Joi.string().required(),
  }),
});
const addCatalog = async (ctx) => {
  const result = await Catalog.query().insert({
    name: ctx.request.body.name,
    ownedBy: ctx.user.id,
  });
  ctx.body = result;
};

const validateAddCatalogItem = validate({
  params: Joi.object({
    id: Joi.number().integer().required(),
  }),
  body: Joi.object({
    name: Joi.string().required(),
  }),
});
const addCatalogItem = async (ctx) => {
  const catalog = await Catalog.query()
    .where({
      id: ctx.params.id,
      ownedBy: ctx.user.id,
    })
    .select('id')
    .first();

  if (!catalog) {
    ctx.status = 404;
    return;
  }

  const result = await CatalogItem.query().insert({
    catalogId: ctx.params.id,
    name: ctx.request.body.name,
  });

  ctx.body = result;
};

const archiveCatalog = async (ctx) => {
  const catalog = await Catalog.query().softDelete().where({
    id: ctx.params.id,
    ownedBy: ctx.user.id,
  });
  await CatalogItem.query().softDelete().where({
    catalogId: ctx.params.id,
  });

  if (catalog > 0) {
    ctx.status = 200;
  } else {
    ctx.status = 404;
  }
};

const archiveCatalogItem = async (ctx) => {
  const {
    params: { catalogId, catalogItemId },
    user,
  } = ctx;

  const itemsAffected = await CatalogItem.query()
    .softDelete()
    .where({
      id: catalogItemId,
    })
    .whereExists(
      CatalogItem.relatedQuery('Catalogs').where({
        id: catalogId,
        ownedBy: user.id,
      })
    );

  if (itemsAffected > 0) {
    ctx.status = 200;
  } else {
    ctx.status = 404;
  }
};

const validateSearchCatalog = validate({
  query: Joi.object({
    q: Joi.string().required().min(1).max(100),
  }),
});

const searchCatalog = async (ctx) => {
  const {
    query: { q },
  } = ctx;
  const results = await Catalog.query()
    .select('name')
    .where('name', 'ilike', `%${q}%`);

  ctx.body = results;
};

const validateSearchCatalogItems = validate({
  body: Joi.object({
    catalogId: Joi.number(),
  }),
  query: Joi.object({
    q: Joi.string().required().min(1).max(100),
  }),
});

const searchCatalogItems = async (ctx) => {
  const {
    query: { q },
    params: { catalogId },
  } = ctx;
  const catalog = await Catalog.query()
    .select('name')
    .where({ id: catalogId, ownedBy: ctx.user.id });
  if (catalog.length < 1) {
    ctx.status = 404;
  }
  const results = await CatalogItem.query()
    .select('name')
    .where('name', 'ilike', `%${q}%`)
    .andWhere('catalogId', catalogId);

  ctx.body = results;
};

module.exports = {
  addCatalog,
  addCatalogItem,
  archiveCatalog,
  archiveCatalogItem,
  read,
  readAll,
  validateRead,
  validateAddCatalog,
  validateAddCatalogItem,
  validateSearchCatalog,
  searchCatalog,
  searchCatalogItems,
  validateSearchCatalogItems,
};
