const validate = (schema) => async (ctx, next) => {
  if (schema.body) {
    const { error, value } = schema.body.validate(ctx.request.body);
    if (error) {
      ctx.status = 400;
      ctx.body = error.details;
      return;
    } else {
      ctx.request.body = value;
    }
  }

  if (schema.params) {
    const { error, value } = schema.params.validate(ctx.params);
    if (error) {
      ctx.status = 400;
      ctx.body = error.details;
      return;
    } else {
      ctx.params = value;
    }
  }

  if (schema.query) {
    const { error, value } = schema.query.validate(ctx.query);
    if (error) {
      ctx.status = 400;
      ctx.body = error.details;
      return;
    } else {
      ctx.query = value;
    }
  }

  await next();
};

module.exports = validate;
