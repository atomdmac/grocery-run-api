const { OAUTH_INTERNAL_URI } = process.env;

// TODO: Can we get rid of this fusion-auth dependency since we're not using it in auth endpoints?
const { FusionAuthClient } = require('@fusionauth/node-client');
const client = new FusionAuthClient('noapikeyneeded', OAUTH_INTERNAL_URI);

const { User } = require('../models');

module.exports = async (ctx, next) => {
  try {
    const accessToken = ctx.cookies.get('access_token');
    const authResponse = await client.retrieveUserUsingJWT(accessToken);
    const {
      successResponse: { user: authUser },
    } = authResponse;

    const appUser = await User.query()
      .select('*')
      .where({
        auth_provider_id: authUser.id,
      })
      .first();

    if (!appUser) {
      throw new Error('Unauthorized');
    }

    // Attach user data to the context so route handlers can use it.
    ctx.user = appUser;
  } catch (error) {
    console.log('Error: ', error);
    ctx.throw(401, 'Unauthorized');
    return;
  }

  await next();
};
