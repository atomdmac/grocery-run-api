const Koa = require('koa');
const Router = require('koa-router');
const BodyParser = require('koa-bodyparser');
const Middleware = require('./middleware');
const Handlers = require('./handlers');
const Routes = require('./routes');
const app = new Koa();

// Logging middleware
app.use(Middleware.Logging.consoleLogger);

// Parsers
app.use(BodyParser());

// Connection Routes
app.use(Handlers.Connection.routes()).use(Handlers.Connection.allowedMethods());

// Auth Routes
app.use(Routes.Auth.routes()).use(Routes.Auth.allowedMethods());

// Webhooks
app.use(Routes.Webhooks.routes()).use(Routes.Webhooks.routes());

// ---
// Authenticated Routes
// ---
const authenticatedRoutes = new Router();
authenticatedRoutes.use(Middleware.OAuth);

// Catalog
authenticatedRoutes
  .use(Routes.Catalog.routes())
  .use(Routes.Catalog.allowedMethods());

// Catalog Item Routes
authenticatedRoutes.use(Routes.List.routes()).use(Routes.List.allowedMethods());
authenticatedRoutes
  .use(Routes.ListItem.routes())
  .use(Routes.ListItem.allowedMethods());

app.use(authenticatedRoutes.routes()).use(authenticatedRoutes.allowedMethods());

app.listen(3000, () => console.log('Server is up on port 3000'));
