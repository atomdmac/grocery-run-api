exports.up = function (knex) {
  return knex.schema.table('Lists', (table) => {
    table.integer('catalogId').references('id').inTable('Catalogs');
  });
};

exports.down = function (knex) {
  return knex.schema.table('Lists', (table) => {
    table.dropColumn('catalogId');
  });
};
