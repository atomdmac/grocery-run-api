exports.up = function (knex) {
  return knex.schema
    .createTable('Users', (table) => {
      table.increments('id').primary();
      table.uuid('auth_provider_id');
      table.string('status', 20).default('active');
      table.timestamps(false, true);
      table.timestamp('deleted_at');
    })

    .createTable('Catalogs', (table) => {
      table.increments('id').primary();
      table.string('name');
      table
        .integer('ownedBy')
        .notNullable()
        .unsigned()
        .references('id')
        .inTable('Users');
      table.timestamps(false, true);
      table.timestamp('deleted_at');
    })

    .createTable('CatalogItems', (table) => {
      table.increments('id').primary();
      table.string('name');
      table
        .integer('catalogId')
        .notNullable()
        .unsigned()
        .references('id')
        .inTable('Catalogs')
        .onDelete('CASCADE');
      table.timestamps(false, true);
      table.timestamp('deleted_at');
    })

    .createTable('Lists', (table) => {
      table.increments('id').primary();
      table.string('name');
      table
        .integer('ownedBy')
        .notNullable()
        .unsigned()
        .references('id')
        .inTable('Users');
      table.timestamps(false, true);
      table.timestamp('deleted_at');
    })

    .createTable('ListItems', (table) => {
      table.increments('id');
      table
        .integer('listId')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('Lists')
        .onDelete('CASCADE');
      table
        .integer('catalogItemId')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('CatalogItems');
      table.timestamps(false, true);
      table.timestamp('deleted_at');
    })

    .createTable('ListPermissions', (table) => {
      table.integer('listId').unsigned().references('id').inTable('Lists');
      table.integer('userId').unsigned().references('id').inTable('Users');
      table.boolean('read').default(true);
      table.boolean('write').default(true);
    });
};

exports.down = function (knex) {
  return knex.schema
    .dropTableIfExists('ListItems')
    .dropTableIfExists('Lists')
    .dropTableIfExists('CatalogItems')
    .dropTableIfExists('Catalogs')
    .dropTableIfExists('Users');
};
