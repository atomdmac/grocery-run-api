const BASE_CONFIG = {
  client: 'postgresql',
  connection: {
    host:     process.env.POSTGRES_HOST,
    database: process.env.POSTGRES_DB,
    user:     process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    port: 5432
  },
  pool: {
    min: 2,
    max: 10
  },
  migrations: {
    directory: './src/migrations',
    tableName: 'knex_migrations'
  }
}

module.exports = {
  development: BASE_CONFIG,
  staging: BASE_CONFIG,
  production: BASE_CONFIG
};
