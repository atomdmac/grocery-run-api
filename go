#!/bin/bash
DEVIMG=grocery_run_api
DC=docker-compose
DOCKER=docker

function start {
  $DC up -d
}

function stop {
  $DC down
}

function restart {
  stop && start
}

function shell {
  $DOCKER exec -it grocery_run_api bash
}

function logs {
  $DC logs -f
}

function migrate {
  $DOCKER exec -it grocery_run_api npx knex migrate:$1
}

function parse_args {
  case "$1" in
    start) start
    ;;
    stop) stop
    ;;
    restart) restart
    ;;
    shell) shell
    ;;
    logs) logs
    ;;
    migrate) migrate "$2"
    ;;
    *) echo "Please input a command."
  esac
}

parse_args "$@"
